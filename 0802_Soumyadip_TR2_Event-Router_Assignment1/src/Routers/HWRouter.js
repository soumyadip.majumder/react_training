import { BrowserRouter, Routes, Route } from "react-router-dom";
import Test2 from "../Test2";
import About from "../About";
import Contact from "../Contact";
export default function HWRouter(){
    return (
        <BrowserRouter>
            <Routes>
                <Route path="" element={<Test2 />}></Route>
                <Route path="/about" element={<About />}></Route>
                <Route path="/contact" element={<Contact/>}></Route>
            </Routes>
        </BrowserRouter>
    );
}